# ConFusion

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# Week 1 Indroduction to Angular

Exercise (Instructions): Getting Started with Angular

### Objectives and Outcomes

In this first Angular exercise, you will first install angular-cli, the command line tool for scaffolding Angular applications. You will then use the tool to scaffold out a basic Angular application. We will thereafter develop this application into a full-fledged Angular application in the process of doing the exercises in this course. At the end of this exercise you will be able to:

* Install angular-cli

* Scaffold out a basic Angular application

* Installing Angular-CLI

From the Angular-CLI documentation we learn that the Angular CLI makes it easy to create an application that already works, right out of the box. It already follows the best practices suggested by the Angular community!

To install angular-cli globally, type the following at the prompt:

** 
npm install -g @angular/cli
**

Use sudo on a Mac and Linux

This will make the command line tool for creating Angular applications. To learn more about the various commands that this CLI provides, type at the prompt:

**
ng help
**

Generating and Serving an Angular Project using Angular-CLI

At a convenient location on your computer, create a folder named Angular and move into that folder.

Then type the following at the prompt to create a new Angular application named conFusion:

**
ng new conFusion -dir=<The path of your Angular folder>/conFusion --style=scss
**

This should create a new folder named conFusion within your Angular folder and create the Angular application in that folder.

Move to the conFusion folder and type the following at the prompt:

**
npm install
**

**
ng serve --open
**

This will compile the project and then open a tab in your default browser at the address http://localhost:4200.

You can initialize your project to be a Git repository by typing the following commands at the prompt:

**
git init
**

**
git add .
**

**
git commit -m "Initial Setup"
**

Note: Some of you may find that Angular CLI automatically does the first commit on your computer and initializes the Git repository. Please do a "git status" in the project folder just to check if an automatic commit has been done. This doesn't happen on my computer. Hence the above instructions.

Thereafter you can set up an online Git repository and synchronize your project to the online repository. Make sure that the online Git repository is a private repository.

Conclusions

In this exercise you installed the Angular CLI tool and created a basic Angular project and served up the compiled project to your browser.

## Exercise (Instructions): Configuring your Angular Application

Objectives and Outcomes

In this exercise we will set up our project to use Angular Material and Angular Flex Layout. We will then introduce our first Angular Material component into our application. At the end of this exercise you will be able to:

Configure your Angular project to use Angular Material and Flex Layout.
Start using Material components in your application.
Configure your Angular Project to use Angular Material

Note: This course is designed with Angular Material Beta.3. Before you proceed forward, you may wish to read the detailed information posted in https://www.coursera.org/learn/angular/discussions/all/threads/4yxVk7DXEee0mQrUfDuicA where I have clearly explained about dealing with the newer Beta versions of Angular Material (up to Beta.12). I would strongly suggest that to proceed ahead with the course with minimal disruption, please install the Beta.8 version of Angular Material. With this installation, the course instructions will still work as given.

To configure your project to use Angular material, type the following at the prompt to install Angular Material, Angular Animations and HammerJS:

**
npm install @angular/material@2.0.0-beta.8 --save
**

**
npm install @angular/cdk@2.0.0-beta.8 --save
**

**
npm install --save @angular/animations 
**

**
npm install --save hammerjs 
**

Configure to use Material Design Icons

Next, include the following into the <head> of index.html to make use of Material Design icons:


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel
  ="stylesheet"> 

Configure your Angular Project to use Flex Layout

Next, install Angular Flex Layout as follows:

**
npm install --save @angular/flex-layout@latest 
**

Updating AppModule

Then, you need to import the Angular Animations Module, Angular Material Module, Flex Layout Module and hammerjs into your root module (src/app/app.module.ts) as follows:

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '@angular/material'; 

import { FlexLayoutModule } from '@angular/flex-layout';
. . . 

import 'hammerjs';

@NgModule({
  
  . . . 
  
  imports: [ 
    
    . . .,
    
    BrowserAnimationsModule,
   
    MaterialModule,
   
    FlexLayoutModule
    
  ], 
    
    . . . 
  
  
}) 
. . . 

Adding a Material Toolbar

Open app.component.html and replace its contents with the following code:


<md-toolbar color="primary"> <span>Ristorante Con Fusion</span> </md-toolbar>

Adding Styles

Add the following styles to styles.scss file:

@import '~@angular/material/prebuilt-themes/deeppurple-amber.css';

// some basic resets 

body { 

  padding: 0; 

  margin: 0; 

  font-family: Roboto, sans-serif; 
  
}

This will add a built-in Material theme to our application.
Do a Git commit with the message "Configuring Angular"

Conclusions

In this exercise we learnt to use Angular Material and Flex Layout NgModules in our Angular application.


## Exercise (Instructions): Angular Components Part 1

Exercise Resources

Objectives and Outcomes

In this exercise you will add the first component to your Angular application and update its template. At the end of this exercise you will be able to:

* Add components to your Angular application

* Update the templates of your component.

Adding a Menu Component

First, download the images.zip file provided above and then unzip the file. Move the resulting images folder containing some PNG files to the Angular project's src/assets folder. These image files will be useful for our exercises.

Next, use the CLI's ng generate command to generate a new component named menu as follows:

**
ng generate component menu
**

This will create the necessary files for the menu component in a folder named menu, and also import this component into app.module.ts.

Next, open app.component.html file and add the following after the toolbar:

**
<app-menu></app-menu>
**

Creating the Menu

Next, create a folder named shared under the src/app folder. To this folder, add a file named dish.ts with the following code:


export class Dish {

    name: string;

    image: string;

    category: string;

    label: string;

    price: string;

    description: string;

}

Update menu.component.ts as follows to add in the data for four menu items:

. . .

import { Dish } from '../shared/dish';

. . .

export class MenuComponent implements OnInit {

  dishes: Dish[] = [

                         {

                           name:'Uthappizza',

                           image: '/assets/images/uthappizza.png',

                           category: 'mains',

                           label:'Hot',

                           price:'4.99',

                           description:'A unique combination of Indian Uthappam 

                             (pancake) and Italian pizza, topped with Cerignola 

                             olives, ripe vine cherry tomatoes, Vidalia onion, 

                             Guntur chillies and Buffalo Paneer.'               

                                      },

                        {

                           name:'Zucchipakoda',

                           image: '/assets/images/zucchipakoda.png',

                           category: 'appetizer',

                           label:'',

                           price:'1.99',

                           description:'Deep fried Zucchini coated with mildly 

                             spiced Chickpea flour batter accompanied with a 

                             sweet-tangy tamarind sauce'                        

                             },

                        {

                           name:'Vadonut',

                           image: '/assets/images/vadonut.png',

                           category: 'appetizer',

                           label:'New',

                           price:'1.99',

                           description:'A quintessential ConFusion experience, 

                             is it a vada or is it a donut?'                    

                                 },

                        {

                           name:'ElaiCheese Cake',

                           image: '/assets/images/elaicheesecake.png',

                           category: 'dessert',

                           label:'',

                           price:'2.99',

                           description:'A delectable, semi-sweet New York Style 

                             Cheese Cake, with Graham cracker crust and spiced 

                             with Indian cardamoms'                        }

                        ];

. . .

}

Next, update the menu.component.html template as follows:

<div class="container"

     fxLayout="column"

     fxLayoutGap="10px">

<md-list fxFlex>

  <md-list-item *ngFor="let dish of dishes">

    <img md-list-avatar src={{dish.image}} alt={{dish.name}}>

    <h1 md-line> {{dish.name}} </h1>

    <p md-line>

      <span> {{dish.description}} </span>

    </p>

  </md-list-item>

</md-list>

</div>

Add the following CSS class to styles.scss file:

.container {

    margin: 20px;

    display:flex;

}

Save all changes and do a Git commit with the message "Components Part 1".

Conclusions

In this exercise we added a new component to our Angular application, added data to its class, and then updated the component template to show the information in the web page.

## Exercise (Instructions): Angular Components Part 2

Objectives and Outcomes

In this exercise we will continue modifying the component template from the previous exercise. Instead of a list, we will use a grid list Angular material component to display the menu in a different way. Also we will use the Card component to display the details of a selected dish. At the end of this exercise you will be able to:

Make use of the Angular material grid list component to display a list of items.
Use the material Card component to display detailed information.
Use a built-in Angular pipe to turn a word into uppercase in the template.
Updating the Menu Template

Open menu.component.html and update its content as follows:

<div class="container"

     fxLayout="column"

     fxLayoutGap="10px">

  <div fxFlex>

    <div>

      <h3>Menu</h3>

      <hr>

    </div>

  </div>

  <div fxFlex>

    <md-grid-list cols="2" rowHeight="200px">

      <md-grid-tile *ngFor="let dish of dishes">

        <img height="200px" src={{dish.image}} alt={{dish.name}}>

        <md-grid-tile-footer>

          <h1 md-line>{{dish.name | uppercase}}</h1>

        </md-grid-tile-footer>

      </md-grid-tile>

    </md-grid-list>

  </div>

</div>

Here we are using the Grid list Angular material component to display the information.
Also, update the menu.component.ts file as follows to move the details of the dishes into a constant, in preparation for introducing services in a future exercise:

 . . .
 
 const DISHES: Dish[] = [

 . . .
 
 ];
 
 . . .
 
 export class MenuComponent implements OnInit {

  dishes = DISHES;

  selectedDish = DISHES[0];

 . . .
 
 }

Add a Card Component

Update the menu.component.html template to display the details of a selected dish using the Material Card component as follows:

  <div fxFlex *ngIf="selectedDish">

    <md-card>

      <md-card-header>

        <md-card-title>

          <h3>{{selectedDish.name | uppercase}}</h3>

        </md-card-title>

      </md-card-header>

      <img md-card-image src={{selectedDish.image}} alt={{selectedDish.name}}>

      <md-card-content>

        <p>{{selectedDish.description}}

        </p>

      </md-card-content>

      <md-card-actions>

        <button md-button>LIKE</button>

        <button md-button>SHARE</button>

      </md-card-actions>

    </md-card>

  </div>

Save the changes and do a Git commit with the message "Components Part 2".

Conclusions

In this exercise we used a grid list to display the information in the menu template. Also we used a card to display the details of a selected dish.